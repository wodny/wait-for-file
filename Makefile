PROGRAM_VERSION:=0.0.0
CFLAGS:=-Wall -pedantic -std=gnu99

all: compile

compile: dist/wait-for-file

dist/wait-for-file: src/wait-for-file.c dist
	gcc -D PROGRAM_VERSION=$(PROGRAM_VERSION) $(CFLAGS) $< -o $@

dist:
	mkdir -p dist

.PHONY: clean

clean:
	rm -rf dist
