#include <argp.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#ifdef PROGRAM_VERSION
#define PROGRAM_VERSION_STR TOSTRING(PROGRAM_VERSION)
#else
#define PROGRAM_VERSION_STR "unknown"
#endif

const char *argp_program_version = "wait-for-file\n" PROGRAM_VERSION_STR;
const char *argp_program_bug_address = "Kamil Krawczyk <kamil.krawczyk@o2.pl>";
struct arguments {
        char *path;
        bool verbose;
};

error_t parse_opt(int key, char *optarg, struct argp_state *state) {
        struct arguments *arguments = state->input;
        switch (key) {
                case ARGP_KEY_ARG:
                        if (state->arg_num >= 1)
                                argp_usage (state);
                        arguments->path = optarg;
                        break;
                case ARGP_KEY_END:
                        if (state->arg_num < 1)
                                argp_usage (state);
                        break;
                default:
                        return ARGP_ERR_UNKNOWN;
        }
        
        return 0;
}

void parse_options(int argc, char **argv, struct arguments *arguments) {
        const char *doc = "The program will be closed successfully if the specified file exists or appear.";
        static char args_doc[] = "FILE";
        static struct argp_option options[] = {
                { 0 }
        };
        struct argp argp = {options, parse_opt, args_doc, doc};
        argp_parse(&argp, argc, argv, 0, NULL, arguments);
}

int main(int argc, char** argv) {
        struct arguments arguments;
        
        arguments.verbose = false;
        
        parse_options(argc, argv, &arguments);
        
        char *dirc = strdup(arguments.path);
        char *basec = strdup(arguments.path);
        char *dname = dirname(dirc);
        char *bname = basename(basec);
        
        int inotifyFd = inotify_init();
        if(inotifyFd == -1) {
                printf("inotify_init\n");
                return (EXIT_FAILURE);
        }
        
        int wd = inotify_add_watch(inotifyFd, dname, IN_CLOSE_WRITE|IN_MOVED_TO|IN_ONLYDIR);
        if (wd == -1) {
                printf("inotify_add_watch\n");
                return (EXIT_FAILURE);
        }
        
        if(access(arguments.path, F_OK) == 0) {
                printf("SUCCESS!\n");
                return (EXIT_SUCCESS);
        }
        
        const struct inotify_event *event;
        char buf[4096]
                __attribute__ ((aligned(__alignof__(struct inotify_event))));
        char *iptr;
        ssize_t len;
        while(1) {
                len = read(inotifyFd, buf, sizeof (buf));
                for(iptr = buf; iptr < buf + len; iptr += sizeof(struct inotify_event) + event->len) {
                        event = (const struct inotify_event*)iptr;
                        printf("inotify_event: %s\n", event->name);
                        if(strcmp(bname, event->name) == 0) {
                                printf("SUCCESS!\n");
                                return (EXIT_SUCCESS);
                        }
                }
        }
        
        return (EXIT_SUCCESS);
}

